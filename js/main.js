// создаём елементы таблицы
const table = document.createElement("table");
const body = document.createElement("body");

// делаем цыкль с помощью его линии и колонка
for (let i = 0; i < 30; i++) {
  const tegTr = document.createElement("tr");
  for (let a = 0; a < 30; a++) {
    const tegTd = document.createElement("td");
    tegTr.appendChild(tegTd);
  }
  body.appendChild(tegTr);
}

// линии и колонки в таблицу и таблицу в бади добвляем
table.appendChild(body);
document.body.appendChild(table);

// добавляем слик на таблицу
table.addEventListener("click", function (event) {
  if (event.target.tagName === "TD") {
    const tableTd = event.target;

    if (tableTd.classList.contains("black")) {
      tableTd.classList.remove("black");
    } else {
      tableTd.classList.add("black");
    }
  }
});

// добавляем слик на боди
document.body.addEventListener("click", function (event) {
  if (!table.contains(event.target)) {
    const tdBody = table.querySelectorAll("td");

    for (let i = 0; i < tdBody.length; i++) {
      tdBody[i].classList.toggle("black");
    }
  }
});
